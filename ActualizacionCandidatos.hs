--4243015 4549396

module ActualizacionCandidatos where

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Tacticas
import FuncionesAuxiliares
import Types


{- Actualiza el sudoku segun el nuevo ocupado -}
actualizarSudoku  :: Ocupado -> Sudoku -> Sudoku
actualizarSudoku ocupado@(pos,num) (libres,ocupados) = ( nuevosLibres , nuevosOcupados)
	where
		libresAux 		  = filter (\x -> (fst x) /= pos) libres
		nuevosOcupados 	  = ocupados ++ [ (pos,num) ]
		nuevosLibres 	  = mergeOrd ( libresSinLibres libresAux libresMismoAmbito ) actualizados
		libresMismoAmbito = (libresEnMismoAmbito (pos,IntSet.empty) libresAux )
		actualizados 	  =	[ (pos,(IntSet.delete num cand)) | (pos,cand) <- libresMismoAmbito ]


aplicarNaked :: Sudoku -> Libres -> IntSet -> Sudoku
aplicarNaked (libres,ocupados) libresQuitar candidatos = (nuevosLibres,ocupados)
	where
		nuevosLibres = mergeOrd (libresSinLibres libres libresQuitar) libreSinCandidatos
		libreSinCandidatos = [ (pos,(IntSet.difference cand candidatos)) | l3@(pos,cand) <- libresQuitar ]


aplicarHiddenPairs :: Sudoku -> Libre -> Libre -> IntSet  -> Sudoku
aplicarHiddenPairs (libres,ocupados) l1@(p1,_) l2@(p2,_) candidatos = (nuevosLibres,ocupados)
	where
		nuevosLibres = mergeOrd [(p1,candidatos),(p2,candidatos)] (libresSinLibres libres [l1,l2])


sacarDigitoAfectados :: Sudoku -> Int -> Libres -> Sudoku
sacarDigitoAfectados sudoku@(libres,ocupados) digito afectados = (nuevosLibres, ocupados)
	where
		nuevosLibres 	   = mergeOrd (libresSinLibres libres afectados) afectadosSinDigito
		afectadosSinDigito = [ (pos,(IntSet.delete digito cand)) | (pos,cand) <- afectados ]



