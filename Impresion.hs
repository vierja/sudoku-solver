--4243015 4549396

module Impresion where

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Tacticas
import Types	

printSudokuFile :: String -> Sudoku -> IO ()
printSudokuFile outputFile (l, ocup) = writeFile outputFile (printOcupados (0, 0) ocup)

printSudoku :: Sudoku -> IO ()
printSudoku (l, ocup) = putStr ( "\n" ++  (printOcupados (0, 0) ocup) )

printOcupados :: Posicion -> Ocupados -> String
printOcupados (row, column) ocup 
  | row > 8 = ""
  | (row == 8) && (column == 8) = toStringInt value
  | (column < 8) && (row < 9) = toStringInt value ++ " " ++ printOcupados (row, column+1) ocup
  | column == 8 = toStringInt value ++ "\n" ++ printOcupados (row+1, 0) ocup
  where value = getFromOcupados (row, column) ocup

toStringInt :: Int -> String
toStringInt 0 = "."
toStringInt i = show i

getFromOcupados :: Posicion -> Ocupados -> Int
getFromOcupados pos [] = 0;
getFromOcupados pos (o:os)
  | fst(o) == pos = snd(o)
  | os /= [] = getFromOcupados pos os
  | otherwise = 0

toStringIntSet :: [Int] -> String
toStringIntSet [] = ""
toStringIntSet (l:ls) = (show l) ++ (toStringIntSet ls)

toStringLibres :: Libres -> String
toStringLibres [] = ""
toStringLibres (p:ps) = "," ++ (show (fst p)) ++ (toStringLibres ps)
