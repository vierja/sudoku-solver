--4243015 4549396

module Types where
	
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet

type Sudoku = (Libres,Ocupados)

type Libre = (Posicion, Candidatos)

type Libres = [Libre]

type Ocupado = (Posicion, Int)

type Ocupados = [Ocupado]

type Posicion = (Int, Int) {- (fila,columna) -}

type Candidatos = IntSet

type Region = (Int, Int)
