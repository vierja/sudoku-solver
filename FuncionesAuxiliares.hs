--4243015 4549396

module FuncionesAuxiliares where

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Types

mismaFila :: Libre -> Libre -> Bool
mismaFila ((f1,c1),_) ((f2,c2),_) = f1 == f2

mismaColumna :: Libre -> Libre -> Bool
mismaColumna ((f1,c1),_) ((f2,c2),_) = c1 == c2

mismaRegion :: Libre -> Libre -> Bool
mismaRegion (pos1,_) (pos2,_) = posicionesMismaRegion pos1 pos2

mismoAmbito :: Libre -> Libre -> Bool
mismoAmbito l1 l2 = mismaFila l1 l2 || mismaColumna l1 l2 || mismaRegion l1 l2

posicionesMismoAmbito :: Posicion -> Posicion -> Bool
posicionesMismoAmbito pos1@(f1,c1) pos2@(f2,c2) = f1 == f2 || c1 == c2 || posicionesMismaRegion pos1 pos2

mismaFila3 :: Libre -> Libre -> Libre -> Bool
mismaFila3 l1 l2 l3 = mismaFila l1 l2 && mismaFila l1 l3

mismaColumna3 :: Libre -> Libre -> Libre -> Bool
mismaColumna3 l1 l2 l3 = mismaColumna l1 l2 && mismaColumna l1 l3

mismaRegion3 :: Libre -> Libre -> Libre -> Bool
mismaRegion3 l1 l2 l3 = mismaRegion l1 l2 && mismaRegion l1 l3

mismaFila4 :: Libre -> Libre -> Libre -> Libre -> Bool
mismaFila4 l1 l2 l3 l4 = mismaFila l1 l2 && mismaFila l1 l3 && mismaFila l1 l4

mismaColumna4 :: Libre -> Libre -> Libre -> Libre -> Bool
mismaColumna4 l1 l2 l3 l4 = mismaColumna l1 l2 && mismaColumna l1 l3 && mismaColumna l1 l4

mismaRegion4 :: Libre -> Libre -> Libre -> Libre -> Bool
mismaRegion4 l1 l2 l3 l4 = mismaRegion l1 l2 && mismaRegion l1 l3 && mismaRegion l1 l4

posicionesMismaRegion :: Posicion -> Posicion -> Bool
posicionesMismaRegion pos1 pos2 = obtenerRegion(pos1) == obtenerRegion(pos2)

parLibresMismaFila :: Libres -> [(Libre,Libre)]
parLibresMismaFila libres =  [ (l1,l2) | l1@(p1,_) <- libres, l2@(p2,_) <- libres, mismaFila l1 l2, p1 < p2 ]

parLibresMismaCol :: Libres -> [(Libre,Libre)]
parLibresMismaCol libres =  [ (l1,l2) | l1@(p1,_) <- libres, l2@(p2,_) <- libres, mismaColumna l1 l2, p1 < p2 ]

parLibresMismaReg :: Libres -> [(Libre,Libre)]
parLibresMismaReg libres =  [ (l1,l2) | l1@(p1,_) <- libres, l2@(p2,_) <- libres, mismaRegion l1 l2, p1 < p2 ]

tripleLibresMismaFila :: Libres -> [(Libre,Libre,Libre)]
tripleLibresMismaFila libres =  [ (l1,l2,l3) | l1@(p1,_) <- libres, l2@(p2,_) <- libres,l3@(p3,_) <- libres, mismaFila3 l1 l2 l3, p1 < p2, p2 < p3]

tripleLibresMismaCol :: Libres -> [(Libre,Libre,Libre)]
tripleLibresMismaCol libres =  [ (l1,l2,l3) | l1@(p1,_) <- libres, l2@(p2,_) <- libres,l3@(p3,_) <- libres, mismaColumna3 l1 l2 l3, p1 < p2, p2 < p3]

tripleLibresMismaReg :: Libres -> [(Libre,Libre,Libre)]
tripleLibresMismaReg libres =  [ (l1,l2,l3) | l1@(p1,_) <- libres, l2@(p2,_) <- libres,l3@(p3,_) <- libres, mismaRegion3 l1 l2 l3, p1 < p2, p2 < p3]

libresSinLibres :: Libres -> Libres -> Libres
libresSinLibres libres [] = libres
libresSinLibres libres (l:ls) = libresSinLibres (filter (\x -> x /= l) libres) ls

libresEnMismaFila :: Libre -> Libres -> Libres
libresEnMismaFila l1 libres = [ l2 | l2 <- libres, mismaFila l1 l2, l1 /= l2 ] 

libresEnMismaColumna :: Libre -> Libres -> Libres
libresEnMismaColumna l1 libres = [ l2 | l2 <- libres, mismaColumna l1 l2, l1 /= l2 ] 

libresEnMismaRegion :: Libre -> Libres -> Libres
libresEnMismaRegion l1 libres = [ l2 | l2 <- libres, mismaRegion l1 l2, l1 /= l2 ] 

libresEnMismoAmbito :: Libre -> Libres -> Libres
libresEnMismoAmbito l1 libres = [ l2 | l2 <- libres, mismoAmbito l1 l2, l1 /= l2] 

libresEnMismaFila2 :: Libre -> Libre -> Libres -> Libres
libresEnMismaFila2 l1 l2 libres = [ l3 | l3 <- libres, mismaFila3 l1 l2 l3, l1 /= l3, l2 /= l3]

libresEnMismaColumna2 :: Libre -> Libre -> Libres -> Libres
libresEnMismaColumna2 l1 l2 libres = [ l3 | l3 <- libres, mismaColumna3 l1 l2 l3, l1 /= l3, l2 /= l3]

libresEnMismaRegion2 :: Libre -> Libre -> Libres -> Libres
libresEnMismaRegion2 l1 l2 libres = [ l3 | l3 <- libres, mismaRegion3 l1 l2 l3, l1 /= l3, l2 /= l3]

libresEnMismaFila3 :: Libre -> Libre -> Libre -> Libres -> Libres
libresEnMismaFila3 l1 l2 l3 libres = [ l4 | l4 <- libres, mismaFila4 l1 l2 l3 l4,  l1 /= l4, l2 /= l4, l3 /= l4 ]

libresEnMismaColumna3 :: Libre -> Libre -> Libre -> Libres -> Libres
libresEnMismaColumna3 l1 l2 l3 libres = [ l4 | l4 <- libres, mismaColumna4 l1 l2 l3 l4,  l1 /= l4, l2 /= l4, l3 /= l4 ]

libresEnMismaRegion3 :: Libre -> Libre -> Libre -> Libres -> Libres
libresEnMismaRegion3 l1 l2 l3 libres = [ l4 | l4 <- libres, mismaRegion4 l1 l2 l3 l4, l1 /= l4, l2 /= l4, l3 /= l4 ]


{-Candidatos-}
candidatosFila :: Libre -> Libres -> IntSet
candidatosFila l1 libres = IntSet.unions [ cand | (_,cand) <- (libresEnMismaFila l1 libres)]

candidatosColumna :: Libre -> Libres -> IntSet
candidatosColumna l1 libres = IntSet.unions [ cand | (_,cand) <- (libresEnMismaColumna l1 libres)]

candidatosRegion :: Libre -> Libres -> IntSet
candidatosRegion l1 libres = IntSet.unions [ cand | (_,cand) <- (libresEnMismaRegion l1 libres)]

{- FUNCIONES PARA CANDIDATOS ENCERRADOS -}

--Candidatos en (F INTERSECCION R)
candidatosFilaRegion :: Int -> Posicion -> Libres -> IntSet
candidatosFilaRegion fila region libres = IntSet.unions [ cand | (pos@(f,c),cand) <- libres,  f == fila, (obtenerRegion pos) == region]

--Candidatos en (F - R)
candidatosFilaMenosRegion :: Int -> Posicion -> Libres -> IntSet
candidatosFilaMenosRegion fila region libres = IntSet.unions [ cand | (pos@(f,c),cand) <- libres, f == fila, (obtenerRegion pos) /= region]

--Candidatos en (R - F)
candidatosRegionMenosFila :: Int -> Posicion -> Libres -> IntSet
candidatosRegionMenosFila fila region libres = IntSet.unions [ cand | (pos@(f,c),cand) <- libres, (obtenerRegion pos) == region , f /= fila]

libresR_F :: Int -> Posicion -> Libres -> Libres
libresR_F fila region libres = [ l | l@(pos@(f,c),_) <- libres, (obtenerRegion pos) == region , f /= fila]

libresF_R :: Int -> Posicion -> Libres -> Libres
libresF_R fila region libres = [ l | l@(pos@(f,c),_) <- libres, (obtenerRegion pos) /= region , f == fila]

--Candidatos en (C INTERSECCION R)
candidatosColumnaRegion :: Int -> Posicion -> Libres -> IntSet
candidatosColumnaRegion columna region libres = IntSet.unions [ cand | (pos@(f,c),cand) <- libres,  c == columna, (obtenerRegion pos) == region]

--Candidatos en (C - R)
candidatosColumnaMenosRegion :: Int -> Posicion -> Libres -> IntSet
candidatosColumnaMenosRegion columna region libres = IntSet.unions [ cand | (pos@(f,c),cand) <- libres, c == columna, (obtenerRegion pos) /= region]

--Candidatos en (C - F)
candidatosRegionMenosColumna :: Int -> Posicion -> Libres -> IntSet
candidatosRegionMenosColumna columna region libres = IntSet.unions [ cand | (pos@(f,c),cand) <- libres, (obtenerRegion pos) == region , c /= columna]

libresR_C :: Int -> Posicion -> Libres -> Libres
libresR_C columna region libres = [ l | l@(pos@(f,c),_) <- libres, (obtenerRegion pos) == region , c /= columna]

libresC_R :: Int -> Posicion -> Libres -> Libres
libresC_R columna region libres = [ l | l@(pos@(f,c),_) <- libres, (obtenerRegion pos) /= region , c == columna]

--Obtener numero de region
obtenerRegion :: Posicion -> Posicion
obtenerRegion (fila,columna) = ((fila `div` 3), (columna `div` 3))
	
mergeOrd :: Libres -> Libres -> Libres
mergeOrd [] l2 = l2
mergeOrd l1 [] = l1
mergeOrd (x:xs) (y:ys)
	| (fst x) <= (fst y) = x : mergeOrd xs (y:ys)
	| True = y : mergeOrd (x:xs) ys

mergeOrdExc :: [(Ocupado,String)] -> [(Ocupado,String)] ->  [(Ocupado,String)] 
mergeOrdExc [] l2 = l2
mergeOrdExc l1 [] = l1
mergeOrdExc (x:xs) (y:ys)
	| (fst (fst x)) <= (fst (fst y)) = x : mergeOrdExc xs (y:ys)
	| True = y : mergeOrdExc (x:xs) ys
