--4243015 4549396

module Resolucion where

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Tacticas
import Types
import Impresion
import ActualizacionCandidatos
import FuncionesAuxiliares

{- Generar candidatos al principio -}

{-Obtener los ocupados en el mismo ambito para sacarlos de los candidatos-}
obtenerOcupadosAmbito :: Ocupados -> Posicion -> IntSet
obtenerOcupadosAmbito ocupados posicion = IntSet.fromList [ num | (pos,num) <- ocupados , posicionesMismoAmbito pos posicion]

{-Saco candidatos de los libres-}
candidatosReales :: Libres -> Ocupados -> Libres
candidatosReales [] _ = []
candidatosReales (libre:libres) ocupados = 
	[( posicion, IntSet.difference candidatos candidatosASacar )] ++ candidatosReales libres ocupados
	where 
		posicion = fst libre
		candidatos = snd libre
		candidatosASacar = (obtenerOcupadosAmbito ocupados posicion)

generarCandidatosSudoku :: Sudoku -> Sudoku
generarCandidatosSudoku (libres, ocupados) = ( (candidatosReales libres ocupados) , ocupados)


---- RESOLUCION POR TACTICA

{- Candidato unico -}
resolverCandUnico :: Sudoku -> Ocupado -> IO ()
resolverCandUnico sud unico = do 
	putStrLn ("Unico " ++ (show (fst unico)) ++ " '" ++ (show (snd unico)) ++ "'")
	resolverSudoku (actualizarSudoku unico sud)	
	
{- Candidato exclusivo -}
resolverCandExclusivo :: Sudoku -> (Ocupado,String) -> IO ()
resolverCandExclusivo sud (ocupado@(pos,n),tipo) = do 
	putStrLn ("Exclusivo " ++ (show pos) ++ " '" ++ (show n) ++ "' " ++ tipo)	
	resolverSudoku (actualizarSudoku ocupado sud)	

{- Naked Pairs -}
resolverNakedPairs :: Sudoku -> ((Libre,Libre),Libres) -> IO ()
resolverNakedPairs sud naked@( ( (pos1,candidatos) , (pos2,_) ) , libresAQuitar@(l:ls) ) = do
	putStrLn ("Descubiertos [" ++ (show pos1) ++ "," ++ (show pos2) ++ "] \"" ++ (toStringIntSet (IntSet.elems candidatos)) ++ "\" [" ++ (show (fst l)) ++ (toStringLibres ls) ++ "]")
	resolverSudoku (aplicarNaked sud libresAQuitar candidatos)


{- Hidden Pairs -}
resolverHiddenPairs :: Sudoku -> ( (Libre,Libre) , IntSet ) -> IO ()
resolverHiddenPairs sud ( ( l1@(pos1,_) , l2@(pos2,_) ) , candidatos ) = do
	putStrLn ("Escondidos [" ++ (show pos1) ++ "," ++ (show pos2) ++ "] \"" ++ (toStringIntSet (IntSet.elems candidatos) ) ++ "\"")
	resolverSudoku (aplicarHiddenPairs sud l1 l2 candidatos)

{- Naked Triples -}
resolverNakedTriples :: Sudoku -> ( (Libre,Libre,Libre), Libres, IntSet) -> IO ()
resolverNakedTriples sudoku ( ( (pos1,_), (pos2,_), (pos3,_) ), libresAQuitar@(l:ls), candidatos) = do
	putStrLn ("Descubiertos [" ++ (show pos1) ++ "," ++ (show pos2) ++ "," ++ (show pos3) ++ "] \"" ++ toStringIntSet (IntSet.elems candidatos) ++ "\" [" ++ (show (fst l)) ++ (toStringLibres ls) ++ "]")
	resolverSudoku (aplicarNaked sudoku libresAQuitar candidatos)

{- Encerrado -}
resolverEncerrado :: Sudoku -> ( Posicion, Int, Int, Libres ) -> String -> IO ()
resolverEncerrado sudoku@(libs, ocups) (region, filaColumna, digito, afectados@(l:ls)) tipo = do 
	putStrLn ("Encerrado \"" ++ tipo ++ "\" " ++ (show region) ++ " " ++ (show filaColumna) ++ " '" ++ (show digito) ++ "' [" ++ (show (fst l)) ++ (toStringLibres ls) ++ "]")
	resolverSudoku (sacarDigitoAfectados sudoku digito afectados)


{-
	Las tácticas se ordenan inicialmente según su tipo de acuerdo con el siguiente orden:

	candidato único
	candidato exclusivo
	pares descubiertos
	pares escondidos
	ternas descubiertas
	encerrado fila-región (encerrado en una región dentro de una fila)
	encerrado columna-región (encerrado en una región dentro de una columna)
	encerrado región-fila (encerrado en una fila dentro de una región)
	encerrado región-fila (encerrado en una columna dentro de una región)
	Si hubiera varias tácticas del mismo tipo se ordenan según las siguientes reglas:

	candidato único: se ordena por celda (fila, columna).
	candidato exclusivo: se ordena por celda (fila, columna). Si hubiera un candidato exclusivo para la misma celda en más de un ámbito, se considera en el orden fila, columna, región.
	pares descubiertos: se ordena por fila, columna, región y dentro de éstas por celda.
	pares escondidos: se ordena por fila, columna, región y dentro de éstas por celda.
	ternas descubiertas: se ordena por fila, columna, región y dentro de éstas por celda.
	encerrado fila-región (encerrado en una región dentro de una fila) : se ordena por región y luego por fila. Si hay más de un encerrado por la misma región y fila, se ordena por el dígito.
	encerrado columna-región (encerrado en una región dentro de una columna): Se ordena por región y luego por columna. Si hay más de un encerrado por la misma columna y región, se ordena por el dígito.
	encerrado región-fila (encerrado en una fila dentro de una región): igual que fila-región
	encerrado región-fila (encerrado en una columna dentro de una región): igual que columna-región.
-}

resolverSudoku :: Sudoku -> IO () 
resolverSudoku sudoku@(libres,ocupados)

	| unico 		/= []	= resolverCandUnico 	sudoku 	(head unico)                                 		

	| exclusivo    	/= []	= resolverCandExclusivo sudoku 	(head exclusivo)

	| nakedPar     	/= []	= resolverNakedPairs 	sudoku  (head nakedPar)

    | hiddenPar    	/= [] 	= resolverHiddenPairs 	sudoku 	(head  hiddenPar)

 	| nakedTriples 	/= [] 	= resolverNakedTriples 	sudoku 	(head nakedTriples)   

 	| encerradoRF  	/= []	= resolverEncerrado 	sudoku	(head encerradoRF) "Fila-Region"

 	| encerradoRC 	/= []	= resolverEncerrado 	sudoku	(head encerradoRC) "Columna-Region"

 	| encerradoFR 	/= []	= resolverEncerrado 	sudoku	(head encerradoFR) "Region-Fila"

 	| encerradoCR 	/= []	= resolverEncerrado 	sudoku	(head encerradoCR) "Region-Columna"

  	| otherwise = printSudoku sudoku -- imprime en pantalla

  	where
  		unico 		 = encontrarCandidatoUnico 			sudoku
  		exclusivo 	 = encontrarCandidatoExclusivo 		sudoku
  		nakedPar 	 = getNakedPairs 					sudoku 
  		hiddenPar 	 = obtenerHiddenPairs 				sudoku
  		nakedTriples = getNakedTriples 					sudoku
  		encerradoRF  = encerradoEnRegionDentroDeFila 	sudoku 
  		encerradoRC  = encerradoEnRegionDentroDeColumna sudoku
  		encerradoFR  = encerradoEnFilaDentroDeRegion 	sudoku
  		encerradoCR  = encerradoEnColumnaDentroDeRegion	sudoku

