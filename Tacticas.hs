--4243015 4549396

module Tacticas where 

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Types	
import FuncionesAuxiliares

{- Candidato Unico -}

encontrarCandidatoUnico :: Sudoku -> [Ocupado]
encontrarCandidatoUnico (libres,_) = [ (pos,head (IntSet.elems candidatos)) | (pos,candidatos) <- libres, IntSet.size candidatos == 1]

{- Candidato exclusivo -}

encontrarCandidatoExclusivo :: Sudoku -> [(Ocupado,String)]
encontrarCandidatoExclusivo (libres,_) = mergeOrdExc (mergeOrdExc fila columna) region
	where		
		fila    = [ ( (pos,exclusivo) , "Fila")  | l@(pos,cand) <- libres, let f = IntSet.difference cand (candidatosFila l libres),
																IntSet.size f == 1, let exclusivo = head (IntSet.elems f) ]

		columna = [ ( (pos,exclusivo) , "Columna") | l@(pos,cand) <- libres, let col = IntSet.difference cand (candidatosColumna l libres),
																IntSet.size col == 1, let exclusivo = head (IntSet.elems col) ]

		region  = [ ( (pos,exclusivo) , "Region") | l@(pos,cand) <- libres, let reg = IntSet.difference cand (candidatosRegion  l libres),
																   IntSet.size reg == 1, let exclusivo = head (IntSet.elems reg)]

{- Pares descubiertos - Naked Pairs -}
{-
	Los digitos {a,b} constituyen un par descubierto dentro de celdas X e Y si:
		- a,b estan en un mismo ambito D (fila, columna o region)
		- ambas celdas tiene el mismo conjunto de candidato {a,b}.
			* Cand(X) = Cand(Y) = {a,b}
		- Existen otras celdas en el mismo ambito que tienen como candidatos a 'a' o 'b'
-}
getNakedPairs :: Sudoku -> [ ( (Libre,Libre), Libres ) ]
getNakedPairs (libres,_)  = fila ++ columna ++ region
	where
		fila = [ ( (l1,l2), candidatos) | (l1@(_,cands1),l2@(_,cands2)) <- parLibresMismaFila libres, 
															cands1 == cands2 ,IntSet.size cands1 == 2,
															let libresMismoAmbito = libresEnMismaFila2 l1 l2 libres,
															libresMismoAmbito /= [],
 															let candidatos = [ l3 | l3@(_,cand3) <- libresMismoAmbito, IntSet.size (IntSet.intersection cands1 cand3) > 0],
 															candidatos /= []]
		
		columna = [ ( (l1,l2), candidatos) | (l1@(_,cands1),l2@(_,cands2)) <- parLibresMismaCol libres, 
															cands1 == cands2 ,IntSet.size cands1 == 2,
															let libresMismoAmbito = libresEnMismaColumna2 l1 l2 libres,
															libresMismoAmbito /= [],
 															let candidatos = [ l3 | l3@(_,cand3) <- libresMismoAmbito, IntSet.size (IntSet.intersection cands1 cand3) > 0],
 															candidatos /= []]
		
		region = [ ( (l1,l2), candidatos) | (l1@(_,cands1),l2@(_,cands2)) <- parLibresMismaReg  libres, 
															cands1 == cands2 ,IntSet.size cands1 == 2,
															let libresMismoAmbito = libresEnMismaRegion2 l1 l2 libres,
															libresMismoAmbito /= [],
 															let candidatos = [ l3 | l3@(_,cand3) <- libresMismoAmbito, IntSet.size (IntSet.intersection cands1 cand3) > 0],
 															candidatos /= []]


{- Pares escondidos -}

obtenerHiddenPairs :: Sudoku -> [( (Libre,Libre) , IntSet )]
obtenerHiddenPairs (libres,_) = fila ++ columna ++ region
	where
		fila 	= [ ((l1,l2),candidatos) | (l1@(_,cands1),l2@(_,cands2)) <- parLibresMismaFila libres ,
															 let interseccion = IntSet.intersection cands1 cands2,
															 IntSet.size interseccion  >= 2,
															 let union = IntSet.union cands1 cands2,
															 let libresMismoAmbito = libresEnMismaFila2 l1 l2 libres,
															 libresMismoAmbito /= [],
															 let candidatosMismoAmbito = IntSet.unions [ cands | l3@(_, cands) <- libresMismoAmbito] ,
															 let candidatos = IntSet.difference interseccion candidatosMismoAmbito, 															 
															 IntSet.size ( IntSet.difference union candidatos ) > 0,
															 IntSet.size candidatos == 2 ]

		columna = [ ((l1,l2),candidatos) | (l1@(_,cands1),l2@(_,cands2)) <- parLibresMismaCol libres ,
															 let interseccion = IntSet.intersection cands1 cands2,
															 IntSet.size interseccion  >= 2,
															 let union = IntSet.union cands1 cands2,
															 let libresMismoAmbito = libresEnMismaColumna2 l1 l2 libres,
															 libresMismoAmbito /= [],
															 let candidatosMismoAmbito = IntSet.unions [ cands | l3@(_, cands) <- libresMismoAmbito] ,
															 let candidatos = IntSet.difference interseccion candidatosMismoAmbito, 															 
															 IntSet.size ( IntSet.difference union candidatos ) > 0,
															 IntSet.size candidatos == 2 ]

		region 	= [ ((l1,l2),candidatos) | (l1@(_,cands1),l2@(_,cands2)) <- parLibresMismaReg libres ,
															 let interseccion = IntSet.intersection cands1 cands2,
															 IntSet.size interseccion  >= 2,
															 let union = IntSet.union cands1 cands2,
															 let libresMismoAmbito = libresEnMismaRegion2 l1 l2 libres,
															 libresMismoAmbito /= [],
															 let candidatosMismoAmbito = IntSet.unions [ cands | l3@(_, cands) <- libresMismoAmbito] ,
															 let candidatos = IntSet.difference interseccion candidatosMismoAmbito, 															 
															 IntSet.size ( IntSet.difference union candidatos ) > 0,
															 IntSet.size candidatos == 2 ]


{-
	Ternas descubiertas - Naked Triples y Hidden Triples
		- c1, c2, c3 estan en un mismo ambito.
		- la union de los candidatos de las tres celdas es de {d1,d2,d3}
		- Existen otras celdas en el mismo ambito que tienen como candidatos a d1, d2, o d3.
-}

getNakedTriples :: Sudoku -> [((Libre,Libre,Libre),Libres,IntSet)]
getNakedTriples (libres,_) = fila ++ columna ++ region
	where
		fila 	= [ ( (l1,l2,l3), candidatos, union ) | (l1@(_,cands1),l2@(_,cands2),l3@(_,cands3)) <- tripleLibresMismaFila libres,
											 let union = IntSet.unions  [cands1, cands2, cands3],
											 IntSet.size union == 3,
											 let libresMismoAmbito = libresEnMismaFila3 l1 l2 l3 libres,
											 libresMismoAmbito /= [],
											 let candidatos = [ l4 | l4@(_,cands4) <- libresMismoAmbito, 
											 						 IntSet.size (IntSet.intersection union cands4) > 0], 
											 candidatos /= [] ]
		columna = [ ( (l1,l2,l3), candidatos, union ) | (l1@(_,cands1),l2@(_,cands2),l3@(_,cands3)) <- tripleLibresMismaCol  libres,
											 let union = IntSet.unions  [cands1, cands2, cands3],
											 IntSet.size union == 3,
											 let libresMismoAmbito = libresEnMismaColumna3 l1 l2 l3 libres,
											 libresMismoAmbito /= [],
											 let candidatos = [ l4 | l4@(_,cands4) <- libresMismoAmbito, 
											 						 IntSet.size (IntSet.intersection union cands4) > 0], 
											 candidatos /= [] ]
		region 	= [ ( (l1,l2,l3), candidatos, union ) | (l1@(_,cands1),l2@(_,cands2),l3@(_,cands3)) <- tripleLibresMismaReg  libres,
											 let union = IntSet.unions  [cands1, cands2, cands3],
											 IntSet.size union == 3,
											 let libresMismoAmbito = libresEnMismaRegion3 l1 l2 l3 libres,
											 libresMismoAmbito /= [],
											 let candidatos = [ l4 | l4@(_,cands4) <-  libresMismoAmbito, 
											 						 IntSet.size (IntSet.intersection union cands4) > 0], 
											 candidatos /= [] ]


{- Candidato Encerrado -}


{- 
	Encerrado en una region R dentro de una fila F.
	Condiciones:
		- d aparece como candidato en alguna de las celdas de (R interseccion F)
		- d no aparece como candidato en ninguna de las celdas de F - R
		- d aparece como candidato en alguna celda de R - F

-}

encerradoEnRegionDentroDeFila :: Sudoku -> [ ( Posicion, Int, Int, Libres ) ]
encerradoEnRegionDentroDeFila (libres,_) =		
	[ (region, fila, digito, afectados) | c <- [0..2], fila <- [0..8] , 
										let f 		  	  =  fila `div` 3,
										let region 		  = ( f, c ),
										let candidatosRF  = candidatosFilaRegion 	  fila region libres,
										IntSet.size candidatosRF > 0,
										let candidatosR_F = candidatosRegionMenosFila fila region libres,
										IntSet.size candidatosR_F > 0,
										let candidatos    = IntSet.intersection candidatosRF candidatosR_F,
										IntSet.size candidatos > 0,	
										let candidatosF_R = candidatosFilaMenosRegion fila region libres,
										let digitos    	  = IntSet.difference candidatos candidatosF_R,
										IntSet.size digitos	> 0,
										let digito 		  = head (IntSet.elems digitos) ,
										let libresAfectados = libresR_F fila region libres,
										let afectados 	  = [ libre | libre@(pos,cand) <- libresAfectados, IntSet.member digito cand ] ]


{-
	Encerrado en una region R dentro de una columna C. "Columna-Region"
	Condiciones:
		- d aparece como candidato en alguna de las celdas de (R interseccion C)
		- d no aparece como candidato en ninguna de las celdas de C - R
		- d aparece como candidato en alguna celda de  - F
-}

encerradoEnRegionDentroDeColumna :: Sudoku -> [ ( Posicion, Int, Int, Libres ) ]
encerradoEnRegionDentroDeColumna (libres,_) =		
	[ (region, columna, digito, afectados) | f <- [0..2],  columna <- [0..8] ,
											let c 		  	  = columna `div` 3,
											let region 		  = ( f, c ),
											let candidatosRC  = candidatosColumnaRegion columna region libres,
											IntSet.size candidatosRC > 0,
											let candidatosR_C = candidatosRegionMenosColumna columna region libres,
											IntSet.size candidatosR_C > 0,
											let candidatos    = IntSet.intersection candidatosRC candidatosR_C,
											IntSet.size candidatos > 0,										
											let candidatosC_R = candidatosColumnaMenosRegion columna region libres,
											let digitos    	  = IntSet.difference candidatos candidatosC_R,
											IntSet.size digitos	> 0,
											let digito 		  = head (IntSet.elems digitos) ,
											let libresAfectados = libresR_C columna region libres,
											let afectados 	  = [ libre | libre@(pos,cand) <- libresAfectados, IntSet.member digito cand ] ]
											

{-
	Encerrado en una fila F dentro de una region R. "Region-Fila"
	Condiciones:
		- d aparece como candidato en alguna de las celdas de (F interseccion R)
		- d no aparece como candidato en ninguna de las celdas de (R - F)
		- d no aparece como candidato en alguna celda de (F - R)
-}

encerradoEnFilaDentroDeRegion :: Sudoku -> [ ( Posicion, Int, Int, Libres ) ]
encerradoEnFilaDentroDeRegion (libres,_) =		
	[ (region, fila, digito, afectados) | f <- [0..2], c <- [0..2],  fila <- [0..8] , 
										let region 		  = ( f, c ),
										let candidatosRF  = candidatosFilaRegion fila region libres,
										IntSet.size candidatosRF > 0,
										let candidatosF_R = candidatosFilaMenosRegion fila region libres,
										IntSet.size candidatosF_R > 0,
										let candidatos    = IntSet.intersection candidatosRF candidatosF_R,
										IntSet.size candidatos 	> 0,
										let candidatosR_F = candidatosRegionMenosFila fila region libres,
										let digitos    	  = IntSet.difference candidatos candidatosR_F,
										IntSet.size digitos	> 0,
										let digito 		  = head (IntSet.elems digitos) ,
										let libresAfectados = libresF_R fila region libres,
										let afectados 	  = [ libre | libre@(pos,cand) <- libresAfectados, IntSet.member digito cand ] ]


{-
	Encerrado en una columna C dentro de una region R. "Region-Columna"
	Condiciones:
		- d aparece como candidato en alguna de las celdas de (C interseccion R)
		- d no aparece como candidato en ninguna de las celdas de (R - C)
		- d no aparece como candidato en alguna celda de (C - R)
-}

encerradoEnColumnaDentroDeRegion :: Sudoku -> [ ( Posicion, Int, Int, Libres ) ]
encerradoEnColumnaDentroDeRegion (libres,_) =		
	[ (region, columna, digito, afectados) | f <- [0..2], c <- [0..2],  columna <- [0..8] , 
											let region 		  = ( f, c ),
											let candidatosRC  = candidatosColumnaRegion columna region libres,
											IntSet.size candidatosRC > 0,
											let candidatosC_R = candidatosColumnaMenosRegion columna region libres,
											IntSet.size candidatosC_R > 0,												
											let candidatos    = IntSet.intersection candidatosRC candidatosC_R,
											IntSet.size candidatos 	> 0,
											let candidatosR_C = candidatosRegionMenosColumna columna region libres,
											let digitos    	  = IntSet.difference candidatos candidatosR_C,
											IntSet.size digitos	> 0,
											let digito 		  = head (IntSet.elems digitos) ,
											let libresAfectados = libresC_R columna region libres,
											let afectados 	  = [ libre | libre@(pos,cand) <- libresAfectados, IntSet.member digito cand ] ]

