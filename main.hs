--4243015 4549396

module Main where

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Tacticas
import Types
import Resolucion 
import Control.Monad

{- Construccion Sudoku -}

stringToSudokuLine :: Int -> String -> Sudoku -> Sudoku
stringToSudokuLine fila (a:as) (libres,ocupados)
	| as == [] && a == '0' 	= ( libres ++ [ ( (fila, columna), candidatos ) ], ocupados )
	| as == [] 				= ( libres, ocupados ++ [ ( ( fila, columna ) , ( read [a] ) :: Int ) ] )
	| a == '0' 				= stringToSudokuLine fila as ( libres ++ [ ( ( fila, columna ), candidatos ) ], ocupados )
	| otherwise 			= stringToSudokuLine fila as ( libres, ocupados ++ [ ( (fila, columna), ( read [a] ) :: Int ) ] )
	where
		columna = 8 - length(as)
		candidatos = IntSet.fromList[1..9]

interpretarSudoku :: Int -> [String] -> Sudoku -> Sudoku
interpretarSudoku _ [] sudoku =  sudoku
interpretarSudoku n (x:xs) sudoku = interpretarSudoku (n+1) xs sudAux
    where sudAux = stringToSudokuLine n x sudoku

mainFile :: String -> IO ()
mainFile inputFile = do
    contents <- readFile inputFile
    let sudoAux =  generarCandidatosSudoku (interpretarSudoku 0 (lines contents)([],[]))
    resolverSudoku sudoAux

main  = do
	lineas <- forM [0..8] (\a -> do
					        linea <- getLine
					        return linea)
	resolverSudoku (generarCandidatosSudoku (interpretarSudoku 0 lineas ([],[])))



